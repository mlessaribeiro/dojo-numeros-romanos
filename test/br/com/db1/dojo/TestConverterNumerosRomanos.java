package br.com.db1.dojo;



import org.junit.Assert;
import org.junit.Test;

public class TestConverterNumerosRomanos {
	
	@Test
	public void testConverterNumero1(){
		Assert.assertEquals("I", NumeroRomano.converterArabicoToRomano(1));
	}
	
	@Test
	public void testConverterNumero5(){
		Assert.assertEquals("V", NumeroRomano.converterArabicoToRomano(5));
	}
	
	@Test
	public void testConverterNumero10(){
		Assert.assertEquals("X", NumeroRomano.converterArabicoToRomano(10));
	}
	
	@Test
	public void testConverterNumero2(){
		Assert.assertEquals("II", NumeroRomano.converterArabicoToRomano(2));
	}

	@Test
	public void testConverterNumero3(){
		Assert.assertEquals("III", NumeroRomano.converterArabicoToRomano(3));
	}

	@Test
	public void testConverterNumero6(){
		Assert.assertEquals("VI", NumeroRomano.converterArabicoToRomano(6));
	}
	
	@Test
	public void testConverterNumero7(){
		Assert.assertEquals("VII", NumeroRomano.converterArabicoToRomano(7));
	}
	
	@Test
	public void testConverterNumero8(){
		Assert.assertEquals("VIII", NumeroRomano.converterArabicoToRomano(8));
	}
	
	@Test
	public void testConverterNumero11(){
		Assert.assertEquals("XI", NumeroRomano.converterArabicoToRomano(11));
	}
	
	@Test
	public void testConverterNumero16(){
		Assert.assertEquals("XVI", NumeroRomano.converterArabicoToRomano(16));
	}
	

	@Test
	public void testConverterNumero22(){
		Assert.assertEquals("XXII", NumeroRomano.converterArabicoToRomano(22));
	}
	
	@Test
	public void testConverterNumero28(){
		Assert.assertEquals("XXVIII", NumeroRomano.converterArabicoToRomano(28));
	}
	@Test
	public void testConverterNumero4(){
		Assert.assertEquals("IV", NumeroRomano.converterArabicoToRomano(4));
	}
	
}
