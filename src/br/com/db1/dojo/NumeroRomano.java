package br.com.db1.dojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class NumeroRomano {

	private static final String NUMERO_1_ROMANO = "I";
	private static final String NUMERO_5_ROMANO = "V";
	private static final String NUMERO_10_ROMANO = "X";

	private static final Map<Integer, String> romanoIndoArabico = new HashMap<>();
	private static final int ZERO = 0;
	private static final int TRES = 3;
	
	static{
		romanoIndoArabico.put(1, NUMERO_1_ROMANO);
		romanoIndoArabico.put(5, NUMERO_5_ROMANO);
		romanoIndoArabico.put(10, NUMERO_10_ROMANO);
	}

	public static String converterArabicoToRomano(Integer numeroArabico) {
		ArrayList<Integer> listaNumerosArabicos =  criaColecaoNaoModificada();
		ordenarDecrescente(listaNumerosArabicos);
		
		return maiorNumeroPossivel(listaNumerosArabicos, numeroArabico);
	}

	private static String maiorNumeroPossivel(ArrayList<Integer> listaNumerosArabicos, Integer numeroArabico) {
		for (Integer numero : listaNumerosArabicos) {
			Integer fatorResto = numeroArabico - numero;
			
			if (fatorResto == ZERO) {
				return romanoIndoArabico.get(numero);
			} else if (isPositivo(fatorResto)) {
				return romanoIndoArabico.get(numero) + maiorNumeroPossivel(listaNumerosArabicos, fatorResto);
			} else if (isNegativo(fatorResto) && ((fatorResto * -1) <= TRES) && (numero <= (fatorResto * -1))) {				
				return  maiorNumeroPossivel(listaNumerosArabicos, fatorResto * -1) + romanoIndoArabico.get(numero);
			}
		}		
		
		return null;
	}

	private static boolean isNegativo(Integer fatorResto) {
		return fatorResto < ZERO;
	}

	private static boolean isPositivo(Integer fatorResto) {
		return fatorResto > ZERO;
	}

	private static void ordenarDecrescente(ArrayList<Integer> listaNumerosArabicos) {
		Collections.sort(listaNumerosArabicos);
		Collections.reverse(listaNumerosArabicos);
	}

	private static ArrayList<Integer> criaColecaoNaoModificada() {
		return new ArrayList(Collections.unmodifiableSet(romanoIndoArabico.keySet()));
	}

}
